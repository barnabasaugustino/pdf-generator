"""
Description:
Author:
Email:
"""

from playwright.sync_api import sync_playwright
import os

class PDF(object):
    def __init__(self):
        self.output_file = None

    def get_file_path(self):
        self.output_file = os.path.join('data', 'output.pdf')

        return self

    def pdf_from_url(self, url):
        """
        :return:
        """
        with sync_playwright() as p:
            browser = p.chromium.launch()
            page = browser.new_page()
            page.goto(url)
            page.emulate_media(media="screen")
            # Convert it
            self.get_file_path()
            page.pdf(path=self.output_file,
                     format="A4",
                     margin={"top": "2cm"})
            browser.close()


