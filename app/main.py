from fastapi import FastAPI, Depends
from app.routes import pdf_generator

def create_app(testing=False):
    app = FastAPI()

    # Registering routes
    app = register_routes(app)

    # Testing endpoint
    @app.get("/")
    async def root():
        return {"message": "Hello Bigger Applications!"}

    return app

def register_routes(app):
    app.include_router(pdf_generator.router)
    return app
