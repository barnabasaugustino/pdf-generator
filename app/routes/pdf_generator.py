from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import FileResponse
from utility import worker


router = APIRouter(
    prefix="/pdf",
    tags=["pdf"],
    responses={404: {"description": "Not found"}},
)

@router.post("/convert")
def convert_pdf_from_url():
    """
    Description: Returns a converted pdf
    :return: Url of the converted file
    """
    url = "https://www.bbc.com"
    converter = worker.PDF()
    converter.pdf_from_url(url)
    return "redirect to download"

@router.get("/download", response_class=FileResponse)
async def download_file():
    """
    Description: Returns pdf file
    :return:
    """
    file_worker = worker.PDF()
    file_path = file_worker.get_file_path()
    return FileResponse(file_path)

# @router.post("/upload")
async def upload_file():
    """
    Description: Upload html file to remote server
    :return:
    """
    return  "Upload html file"
