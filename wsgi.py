#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: barnabas
"""

from app.main import create_app
app = create_app()
